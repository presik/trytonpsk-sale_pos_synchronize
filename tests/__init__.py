#This file is part sale_pos module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.

from .test_sale_pos_synchronize import suite

__all__ = ['suite']
