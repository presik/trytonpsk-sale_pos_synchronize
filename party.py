from sql import Null, Table
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.transaction import Transaction


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    external_id = fields.Integer('External Id')

    @classmethod
    def __register__(cls, module_name):
        super(Party, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        table = cls.__table_handler__(module_name)
        party = Table(cls._table)
        cursor.execute(
            *party.select(
                party.id, party.external_id,
                where=party.external_id != Null,
                limit=1))
        if table.column_exist('external_id') and not cursor.fetchone():
            cursor.execute(
                *party.update(
                    [party.external_id], [party.id],
                    where=party.external_id == Null))


class ContactMechanism(metaclass=PoolMeta):
    __name__ = 'party.contact_mechanism'

    external_id = fields.Integer('External Id')

    @classmethod
    def __register__(cls, module_name):
        super(ContactMechanism, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        table = cls.__table_handler__(module_name)
        contact = Table(cls._table)
        cursor.execute(
            *contact.select(
                contact.id, contact.external_id,
                where=contact.external_id != Null,
                limit=1))
        if table.column_exist('external_id') and not cursor.fetchone():
            cursor.execute(
                *contact.update(
                    [contact.external_id], [contact.id],
                    where=contact.external_id == Null))


class Address(metaclass=PoolMeta):
    __name__ = 'party.address'

    external_id = fields.Integer('External Id')

    @classmethod
    def __register__(cls, module_name):
        super(Address, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        table = cls.__table_handler__(module_name)
        address = Table(cls._table)
        cursor.execute(
            *address.select(
                address.id, address.external_id,
                where=address.external_id != Null,
                limit=1))
        if table.column_exist('external_id') and not cursor.fetchone():
            cursor.execute(
                *address.update(
                    [address.external_id], [address.id],
                    where=address.external_id == Null))


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'

    external_id = fields.Integer('External Id')

    @classmethod
    def __register__(cls, module_name):
        super(Employee, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        table = cls.__table_handler__(module_name)
        employee = Table(cls._table)
        cursor.execute(
            *employee.select(
                employee.id, employee.external_id,
                where=employee.external_id != Null,
                limit=1))
        if table.column_exist('external_id') and not cursor.fetchone():
            cursor.execute(
                *employee.update(
                    [employee.external_id], [employee.id],
                    where=employee.external_id == Null))


class Consumer(metaclass=PoolMeta):
    __name__ = 'party.consumer'

    external_id = fields.Integer('External Id')

    @classmethod
    def __register__(cls, module_name):
        super(Consumer, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        table = cls.__table_handler__(module_name)
        consumer = Table(cls._table)
        cursor.execute(
            *consumer.select(
                consumer.id, consumer.external_id,
                where=consumer.external_id != Null,
                limit=1))
        if table.column_exist('external_id') and not cursor.fetchone():
            cursor.execute(
                *consumer.update(
                    [consumer.external_id], [consumer.id],
                    where=consumer.external_id == Null))
